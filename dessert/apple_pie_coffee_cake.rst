Apple Pie Coffee Cake
---------------------

Cake

* 1 box white cake mix
* 1/4 cup water
* 1 teaspoon ground cinnamon
* 21 oz apple pie filling
* 1 cup sour cream
* 3 large eggs

Glaze

* 1 cup powdered sugar
* 1 1/2 Tablespoons milk



- Prep:  Preheat oven to 350˚ F.  Grease and flour a 13×9 baking dish.
- Mix: Using a mixer, beat together the cake mix, cinnamon, sour cream, eggs and water until well combined.  Pour into the greased and flour 13×9 baking dish.

- Pie Filling:  Spoon on pie filling on spoonful at a time evenly distributing over the entire cake batter.
- Bake: Bake for 35 minutes at 350˚ F or until toothpick inserted int he middle comes out clean.  Cool for 10 minutes.
- Glaze: Mix powdered sugar and milk. Drizzle over cake.  Cool completely.


