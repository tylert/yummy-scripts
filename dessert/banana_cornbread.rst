Banana Cornbread
----------------

* 2 cups all-purpose flour
* 2 cups cornmeal
* 2 cups milk
* 2 bananas
* 1/2 cups white sugar
* 3/4 cup vegetable oil
* 2 teaspoons baking powder
* 2 teaspoons salt

- Preheat oven to 350 degrees F (175 degrees C). Grease a 9x13-inch baking dish.
- Combine flour, cornmeal, milk, bananas, sugar, vegetable oil, baking powder, and salt together in a bowl until batter is smooth; pour into the prepared baking dish.
- Bake in the preheated oven until a toothpick inserted in the center comes out clean, about 30 minutes.

