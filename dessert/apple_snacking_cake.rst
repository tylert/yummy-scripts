Apple Snacking Cake
-------------------

* 1 3/4 cups all-purpose flour (210 g)
* 1 cup granulated sugar
* 1/4 cup vegan protein powder (or replace with flour if you prefer)
* 2 tsp baking powder
* 1/2 tsp baking soda
* 2 tsp cinnamon
* 1/4 tsp nutmeg
* 1/4 tsp allspice
* 1/4 tsp ground ginger
* 1/2 tsp salt

* 1 cup dairy-free milk
* 1 tbsp apple cider vinegar
* 1/2 cup unsweetened applesauce
* 1/2 cup neutral oil
* 2 tsp vanilla extract or vanilla bean paste

* 2 apples chopped into small pieces (3 cups / 360 g)

* 1 cup vegan cream cheese
* 1/2 cup powdered sugar
* 2 tbsp vegan protein powder (or replace with powdered sugar)
* 1 tsp vanilla bean paste or vanilla extract

- To a bowl, add the wet ingredients, followed by the dry ingredients. Whisk everything together until well combined. 
- Wash and finely chop the apples. I chopped them into small pieces (about 1/4-inch squares) so they can cook easier.
- Fold them into the cake batter. 
- Transfer the cake batter to a baking pan (I used a 9x9 inch pan). Bake at 350*F for 50-55 minutes, until a toothpick comes out clean. 
- While the cake is baking, whisk together the frosting ingredients. 
- Once the cake has *fully* cooked, spread the frosting on top of the cake. Slice and enjoy!
