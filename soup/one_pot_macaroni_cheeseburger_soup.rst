One Pot Macaroni Cheeseburger Soup
----------------------------------

* 1 lb ground beef
* 1 medium onion, diced
* 2 cups beef broth
* 2 cups elbow macaroni
* 1 cup shredded cheddar cheese
* 1 cup milk
* Salt and pepper to taste


- In a large pot, brown the ground beef and onion over medium heat until cooked through; drain excess fat.
- Add beef broth and bring to a boil.
- Stir in macaroni and cook until al dente, about 7-8 minutes.
- Reduce heat, add milk and cheese, stirring until cheese is melted and soup is creamy.
- Season with salt and pepper to taste before serving.
