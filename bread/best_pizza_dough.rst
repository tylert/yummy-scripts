Best Pizza Dough

TOTAL TIME: Prep: 30 min. + chilling YIELD: 2 crusts (8 servings each)

* 1-1/4 cups warm water (110° to 115°)
* 2 teaspoons sugar, divided
* 1-1/4 tsp active dry yeast
* 2 tbsp oil
* 3-1/2 to 4 cups all-purpose
* 1 teaspoon sea salt
* 1 teaspoon each dried basil, oregano and marjoram, optional


- In a small bowl, mix warm water and 1 teaspoon sugar; add yeast and whisk until dissolved. Let stand until bubbles form on surface. In a large bowl, whisk 3 cups flour, salt, remaining 1 teaspoon sugar and, if desired, dried herbs. Make a well in center; add yeast mixture and oil. Stir until smooth. Add enough remaining flour to form a soft dough.
- Turn onto a floured surface; knead, adding more flour to surface as needed until no longer sticky and dough is smooth and elastic, 6-8 minutes. Place in a large greased bowl; turn once to grease top. Cover and let rise in a warm place for 30 minutes.

