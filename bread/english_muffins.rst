English Muffins
---------------

* 1 cup milk, warmed
* 2 teaspoons sugar
* 2 teaspoons active dry yeast
* 3 cups all-purpose flour, plus more for dusting
* 1/2 teaspoon salt
* 1 egg, beaten
* 2 tablespoons unsalted butter, melted
* Cornmeal for dusting

- In a small bowl, combine the warm milk and sugar, then sprinkle the yeast over the top. Let it sit for 5 minutes or until frothy.
- In a large bowl, mix together the flour and salt. Make a well in the center and add the frothy yeast mixture, beaten egg, and melted butter. Stir until a soft dough forms.
- Turn the dough out onto a floured surface and knead for about 10 minutes until smooth and elastic.
- Place the dough in a greased bowl, cover with a clean kitchen towel, and let it rise in a warm place for 1 hour, or until doubled in size.
- Punch down the dough and roll it out on a lightly floured surface to about 1/2 inch thick. Use a round cutter to cut out muffins.
- Sprinkle a baking sheet with cornmeal and transfer the cut muffins onto it. Cover and let them rise for another 30 minutes.
- Heat a skillet or griddle over medium-low heat. Cook the muffins for about 5-7 minutes on each side, or until they are golden brown and cooked through.
- Split the English muffins with a fork to preserve the nooks and crannies, and serve warm.
