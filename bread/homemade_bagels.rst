Homemade Mini Bagels
--------------------
* 1 cup warm water 105°℉
* 2 1/4 teaspoon active dry yeast
* 1 tablespoon sugar
* 3 cups bread flour
* 1 teaspoon salt
* 1 tablespoon avacado or olive oil neutral in flavor

* 8-10 cups water
* 1-2 teaspoon baking soda for adding a golden brown color to crust

- In a bowl, combine warm water (108° F/42°C) and sugar. Sprinkle yeast over the water and let it sit for 5-10 minutes until frothy.
- In a large mixing bowl, combine bread flour and salt. Add the activated yeast mixture and olive oil. Knead the dough until it becomes smooth and elastic. This usually takes about 8-10 minutes. Place the dough in a lightly oiled bowl, cover it with a damp cloth, and let it rise in a warm place for 1-1.5 hours or until it doubles.
- Preheat your oven to 425°F (220°C). Line a baking sheet with parchment paper.
- Punch down the risen dough and divide it into 12 equal portions. Roll each portion into a ball. Flatten each ball slightly, then poke a hole in the center with your finger. Gently stretch and shape the dough into mini bagels. Place the shaped bagels on the prepared baking sheet. Cover them with a damp cloth and let them rise for another 30 minutes.
- Bring a large pot of water to a boil. Add 1 teaspoon of baking soda to water. Carefully lower each bagel into the boiling water and boil for about 40 to 1 minutes per side.
- Remove the bagels from the boiling water and place them back on the baking sheet. Sprinkle everything topping generously over each bagel. Bake in the oven for 15-20 minutes or until the bagels are golden brown.
- Allow the mini everything bagels to cool on a wire rack before slicing and enjoying them with your favorite spreads or toppings.


