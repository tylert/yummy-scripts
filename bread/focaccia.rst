Easiest Focaccia Bread
----------------------

* 4 cups (512 g) all-purpose flour
* 2 teaspoons (10 g) kosher salt
* 2 teaspoons (8 g) instant yeast
* 2 cups (455 g) lukewarm water
* butter for greasing
* 4 tablespoons olive oil, divided
* flaky sea salt, such as Maldon
* 1 to 2 teaspoons whole rosemary leaves, optional

- Make the dough: In a large bowl, whisk together the flour, salt, and instant yeast. Add the water. Using a rubber spatula, mix until the liquid is absorbed and the ingredients form a sticky dough ball. Rub the surface of the dough lightly with olive oil. Cover the bowl and let rise at room tempurature until it doubles in size (1.5 to 2 hours). If you do not slick the dough with enough oil, you risk the dough drying out and forming a crust over the top layer.

- Line two 8 or 9inch pie plates or a 9×13-inch pan with parchment paper or grease with butter or coat with nonstick cooking spray.

- Pour a tablespoon of oil into the center of each pan or 2 tablespoons of oil if using the 9×13-inch pan. Using two forks, deflate the dough by releasing it from the sides of the bowl and pulling it toward the center. Rotate the bowl in quarter turns as you deflate, turning the mass into a rough ball. Use the forks to split the dough into two equal pieces (or do not split if using the 9×13-inch pan). Place one piece into one of the prepared pans. Roll the dough ball in the oil to coat it all over, forming a rough ball. Repeat with the remaining piece. Let the dough balls rest for 3 to 4 hours depending on the temperature of your kitchen, no need to cover for this rise.

- Set a rack in the middle of the oven and preheat it to 425°F. If using the rosemary, sprinkle it over the dough. Pour a tablespoon of oil over each round of dough (or two tablespoons if using a 9×13-inch pan).  Rub your hands lightly in the oil to coat, then, using all of your fingers, press straight down to create deep dimples. If necessary, gently stretch the dough as you dimple to allow the dough to fill the pan. Sprinkle with flaky sea salt all over.

- Bake for 25 to 30 minutes, until the underside is golden and crisp. Remove the pans or pan from the oven and transfer the focaccia to a cooling rack. Let it cool for 10 minutes before cutting and serving; let it cool completely if you are halving it with the intention of making a sandwich.
