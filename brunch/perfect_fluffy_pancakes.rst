Perfectly Fluffy Pancakes
--------------------------

* 2 cups all-purpose flour
* 2 tablespoons sugar or to taste
* 4 teaspoons baking powder
* 1/2 teaspoon baking soda
* 1/4 teaspoon salt
* 1 3/4 cups milk or as needed
* 2 large eggs
* 2 tablespoons melted butter or vegetable oil
* 1 teaspoon vanilla extract

Instructions

- In a medium bowl, whisk milk, eggs, melted butter, and vanilla.
- In a separate bowl, combine flour, sugar, baking powder, baking soda, and salt. Whisk to combine.
- Create a well in the center of the dry ingredients and add the wet mixture. Gently stir the batter until just combined, the batter should be slightly lumpy.
- Preheat a griddle or frying pan over medium heat (or to 350°F). Lightly grease with butter or vegetable oil.
- Pour about ¼ cup of batter onto the heated griddle for each pancake. Cook on one side until bubbles form and begin to pop, about 2 minutes. Flip the pancakes over and cook for an additional 1-2 minutes, or until both sides are golden brown.
- Transfer the cooked pancakes to a serving plate or a warmed oven and repeat with the remaining batter.
