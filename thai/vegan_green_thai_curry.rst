Vegan Thai Green Curry
----------------------

* 2 tablespoons coconut oil
* 2 tablespoons green curry paste
* 1 red onion chopped
* 4 cloves garlic minced
* 1 head broccoli chopped
* 1/2 medium red bell pepper cut into strips/spears
* 1/2 medium green bell pepper cut into strips/spears
* 3 carrots cut into strips/spears
* 1 zucchini cut into spears
* 3/4 teaspoon sea salt to taste
* 1/2 teaspoon ground black pepper to taste
* 1 tablespoon coconut sugar or raw sugar
* 1 tablespoon liquid aminos or tamari or low sodium soy sauce
* 1 can coconut milk (full fat or lite) 13.5 ounces (383g)
* half a lime
* 1 bunch thai basil washed and chopped (about 2-3 tablespoons)
* 1/2 tablespoon sambal oelek or Sriracha optional, for heat

- In a pan over medium high heat, heat 1 tablespoon of the coconut oil.
- Add in the curry paste to fry it, stirring into the coconut oil, for about 1 minute. Add in the chopped onion and cook until onion is slightly translucent about 8 minutes.
- Add in the garlic, stir together, then the second tablespoon of coconut oil, then add the broccoli, red bell pepper, green bell pepper, carrots and zucchini. Add the sea salt, black pepper, coconut sugar, and liquid aminos on top and stir everything together. Reduce heat to medium and cook down, stirring, until carrots are tender crisp, about 10-15 minutes.
- Add in the coconut milk, stir, and then let it simmer for about 5 minutes. Squeeze the lime over, stir, and then remove from heat.
- Add in the thai basil on top, and stir in the sambal oelek (or Sriracha) if using. Serve with rice, naan bread and a lime wedge. Enjoy!!
