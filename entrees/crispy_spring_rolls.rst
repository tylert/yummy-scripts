Crispy Spring Rolls
-------------------

* 2 cups (1.4 oz) glass noodles, cooked
* 3 tsp (15 ml) soy sauce
* 3/4 tsp ground white pepper
* 1/2 tsp veg soup powder
* 4-5 cloves garlic, minced
* 3 medium size dried shiitake mushrooms, soaked in hot water until soft (at least 30 mins), then finely chopped or 4-5 cremini thinly sliced
* 2 cups finely shredded green cabbage (about 2-inch juliennes)
* 1 cup grated carrots
* 8-10 cilantro stems, finely chopped
* 2-3 Tbsp water, as needed
* 15-20 pieces of 8-inch spring roll wrappers, thawed
* 1 egg, beaten, for sealing the wrapper
* Oil for frying
* Dipping sauce: sweet chili sauce, store bought or homemade


- Soak glass noodles in room temperature water for 7-10 minutes. Drain, then cut into 2-inch pieces.
- In a wok or a large saute pan, add a little vegetable oil and turn the heat to medium. Add garlic, white pepper and veg soup powder; stir until garlic starts to turn colour slightly.
- Add mushrooms, noodles, cabbage, carrots, cilantro stems and 2 tsp soy sauce. Toss until noodles are fully cooked and cabbage is wilted, adding a splash of water if noodles start sticking or if the vegetables are cooked but the noodles feel too chewy.
- Turn off the heat, then taste and add more salt as needed.
- Put the filling into a bowl and let cool before wrapping. While the filling is cooling, peel the wrappers apart so it’s easier when you wrap. (Spring roll wrappers tend to be hard to peel apart right out of the package, so I like to separate them before I start wrapping.)
- Beat one egg in a bowl which you will use to seal the wrapper.
- Use a heaping 1/4 cup of filling per roll.
- To fry, heat at least 2 inches of oil in a pot to 350°F (heat the oil to 375°F if frying from frozen).  Keep the heat on medium or medium high while you fry, then add the spring rolls and fry until golden brown, about 5 minutes. Drain on paper towel and repeat until. If they're not browned after 5 minutes the heat is too low, if they're too browned, the heat is too high.
- Cool slightly before serving.
