Tater Tot Casserole
-------------------

* 1 lb. bulk ground breakfast sausage
* 1 medium yellow onion diced
* 1 red bell pepper diced
* 1 green bell pepper diced
* 2 lbs. frozen tater tots
* 1 1/2 cups shredded sharp cheddar cheese
* 1 1/2 cups shredded pepper jack cheese
* 10 large eggs
* 1 cup whole milk
* 1/2 teaspoon kosher salt
* 1/2 teaspoon fresh ground black pepper


- Preheat oven to 350 degrees.  Spray a 9x13-inch baking dish with nonstick spray.
- In a large skillet over medium-high heat, brown the pork sausage breaking it up with a spatula. When the sausage is about halfway through the browning process, add the onions and bell peppers. Continue cooking until the pork is browned and the onions and peppers are soft. Drain any excess grease and set the pan aside.
- Place a single layer of frozen tater tots in the bottom of the prepared baking dish. Cover the tater tots with half the cheddar cheese and half the Monterey Jack cheese.
- Whisk together the milk, eggs, kosher salt, and fresh ground black pepper. Pour the egg mixture over the shredded cheese. Spoon the sausage mixture over the eggs. Top with any remaining tater tots and bake covered with aluminum foil for 30 minutes.
- Remove the foil and sprinkle with the rest of the cheese. Bake for around 25 minutes or until the cheese is melted and the casserole is set and cooked through. For best results, let it stand for 10 minutes before slicing.
