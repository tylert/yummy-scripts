Yellow Rice
-----------

* 2 cups basmati rice (or long grain rice of choice)
* 3 cups veggie broth
* 1 tablespoon olive oil
* 1 teaspoon turmeric
* 1 teaspoon parsley (fresh or dried)
* 1 teaspoon paprika
* 1 teaspoon onion powder
* 1 teaspoon garlic powder


- Wash and drain rice 2-3 times or until the water runs almost clear.
- On medium heat in a medium sized sauce pot add the olive oil and rice.
- Sauté over medium heat until it starts to toast lightly. Stir in 2.5 cups veggie broth broth and all remaining ingredients. Bring to a boil.
- Once boiling, lower the heat to low and simmer for 15 minutes with the lid on.
- Remove from the heat and set aside. Do not remove the lid of the pot, allow the rice to steam for 15 minutes with the lid on.
