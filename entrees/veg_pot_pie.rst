Best Vegetarian Pot Pie
-----------------------

* 1/2 cup butter
* 1 medium onion peeled and chopped
* 3-4 cloves garlic minced
* 1 1/2 cups chopped carrots
* 1 1/2 cups chopped potatoes peeled and cut into 1/2 inch chunks
* 1/4 cup all-purpose flour
* 1/2 teaspoon thyme
* 1/2 teaspoon rosemary
* 2 cups vegetable stock
* 1 cup frozen peas
* 2 tablespoons fresh chopped parsley
* 1/4 cup heavy cream
* 1 sheet of puff pastry
* Salt and pepper
* Eggwash 1 egg + 1 tablespoon water

- Preheat the oven to 400 degrees F. Chop all the fresh produce.
- Set a large sauté pan over medium heat. Add the butter. Once melted, add the onion and garlic. Sauté for 2-3 minutes.
- Then add in the carrots and potatoes. Sauté another 5 minutes to soften the vegetables.
- Stir in the flour, thyme, rosemary, 1 teaspoon salt, and 1/2 teaspoon cracked black pepper. Make sure the flour is evenly coating all the vegetables. Then stir in the broth.
- Let the filling simmer for 3-5 minutes to thicken. Then turn off the heat and stir in the peas, parsley, and heavy cream.
- Pour filling into appropriate sized casserole dish.
- Place puff pastry over filling and brush with eggwash. Then use a small knife to cut 3-4 vent holes in the top crust.
- Bake on the bottom rack for 20 minutes or until golden brown.
- Remove the pie from the oven. Cool for 15 minutes before cutting to serve.
