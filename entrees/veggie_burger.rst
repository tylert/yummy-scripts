Veggie Burger
-------------

* 2 cups cooked white rice (1 cup uncooked)
* 2 large garlic cloves
* 1 medium yellow onion
* 3 tablespoons olive oil, divided
* 1 15-ounce can chickpeas
* 3/4 cup whole wheat flour (or gluten free)
* 1 tablespoon cumin
* 1 1/2 teaspoons garlic powder
* 1 1/2 teaspoons onion powder
* 1 1/2 teaspoons kosher salt
* 1 teaspoon black pepper
* 1 egg white**


- Make the rice according to the package instructions or use our Instant Pot method. If making the rice directly before grilling, quickly cool it by spreading the rice into a single layer on a sheet pan and placing it in the freezer for 2 minutes until it cools. Otherwise, you can make the rice in advance and refrigerate until using.
- Preheat a grill to medium heat.*
- Mince the garlic and onion. In a large skillet, heat 1 tablespoon olive oil. Add the the onion and saute for 3 minutes, then add the garlic and saute for for 2 more minutes until golden and translucent. Remove from the heat.
- Drain and rinse the chickpeas. Place them in the bottom of a large bowl and mash them with a fork. Mash until about 90% of the garbanzo beans are mashed; a few can remain whole.
- Measure out 2 cups of the rice and add it to the bowl with the garbanzo beans. Then add the onion and garlic mixture, 2 tablespoons olive oil, flour, cumin, garlic powder, onion powder, kosher salt, black pepper. Mix together, smashing any rice clumps so that everything mixes evenly. Stir in 1 egg white, then mix until everything comes together into a sticky dough.
- Using your hands, form five or six 1-inch thick patties by firmly patting and squeezing the dough together (making 5 results in a very thick burger; making 6 makes moderate-sized burgers). Set the patties on a baking sheet or plate for grilling. Brush both sides of the burgers with olive oil.
- Place the burgers on a grill and grill for 10 to 12 minutes on one side, until firm and grill marks appear. Gently flip the burgers and brush them with barbecue sauce. Grill for an additional 10 to 12 minutes on the other side, then serve immediately.
