﻿Easy Dirty Rice
---------------

* 1 package frozen veggie ground beef
* 2 veggie sausages
* 1 medium yellow onion, diced (about 1 cup)
* 1 green bell pepper, diced
* 2 ribs celery, diced
* 2 teaspoons garlic, minced
* 1 tablespoon Cajun seasoning
* 1/2 teaspoon kosher salt
* 1/2 teaspoon black pepper
* 2 cups long-grain white rice, uncooked
* 2 tablespoons homemade veggie soup mix
* 4 cups water
* fresh parsley, chopped for garnish


- Defrost the veggie ground beef.
- Meanwhile, sautee onions, green pepper, and celery. Add garlic and cook for 1 more minute.
- Stir in Cajun seasoning, kosher salt, and pepper until well combined.
- Add uncooked rice, water and veggie soup mix. Bring the mixture to a boil.
- Reduce heat to low, cover, and simmer for about 20 minutes, or until rice is cooked through. Stir in veggie ground beef.
- Remove from heat. Allow the rice to rest for 5 minutes, then fluff with a fork.
- Garnish with fresh parsley before serving.
