Guyanese Curry
--------------

For the homemade green seasoning
* 1 medium onion, roughly chopped
* 10 cloves garlic, peeled
* 2 wiri wiri peppers or 1 small Scotch bonnet or 1 habanero
* 2 scallions
* 2-3 tablespoons water

For the curry paste
* 2 tablespoons green seasoning
* 1 1/2 teaspoons garam masala
* 1 1/2 teaspoons Madras curry powder
* 1/2 teaspoon turmeric powder
* 1/2 teaspoons geera (ground roasted cumin)
* 1/4 cup water

For the curry
* 1/2 cup neutral cooking oil such as canola, vegetable, or avocado
* 1 small yellow onion, diced
* 1 small scotch bonnet, habanero, 2 wiri wiri peppers
* 6 cloves
* 1/2 inch piece cinnamon stick
* 1 small bay leaf
* 1 tablespoon tomato paste
* 1 1/2 teaspoons salt, plus more if needed
* 4-6 cups boiling water
* 2 medium russet potatoes, peeled and chopped lengthwise into 5-6 slices
* 3 scallions, thinly sliced
* 1/2 teaspoon geera (ground roasted cumin)

Make the green seasoning:
 - In a blender or food processor add the onion, garlic, pepper, scallions, and water. Blend into a paste. The consistency should be thick like a smooth pesto sauce.
- Set aside 2 tablespoons for this recipe and store the remainder in the freezer for other uses. Place green seasoning into an airtight freezer safe container or divide it up into silicone ice cube trays. When ready to use, just pop one out of the tray, melt in the microwave and use as needed.
- The green seasoning can be used as a base to season other meat or rice dishes.
- Dry the meat with a paper towel. Add the garam masala, curry powder, and the green seasoning. Massage into the meat, cover with plastic wrap or a clean dish towel, and set aside to marinate for a minimum of 30 minutes.

Make the curry paste:
- In a small bowl combine the garam masala, curry powder, turmeric powder, geera, 2 tablespoons green seasoning, and water. Set aside.

Cook the curry paste:
- Heat the oil in a Dutch oven over medium heat. Add the curry paste and cook, stirring constantly, until the curry paste starts to look dry and a little browned. Add the chicken and  stir to coat in the curry paste.
Add the remaining ingredients:
- Add the onions, pepper, clove, cinnamon, bay leaf, tomato paste, and salt. Stir to combine and let cook, stirring occasionally, for 15 to 20 minutes. While the chicken is cooking it will release some liquid; let the chicken simmer in its juices until just a little is left. The chicken will begin to look dry with the spices almost seared on it.
- Pour in enough boiling water to come halfway up the pot.
- Remove potatoes from the water and add to the curry. Stir to combine.

Let the curry simmer:
- Reduce the heat to medium-low. Let the curry simmer uncovered, stirring occasionally, until the gravy has reduced and starts to look thick, about 20 to 25 minutes.
- Add the scallions and geera and stir to combine. Remove from heat. Taste and adjust with additional salt if needed.
