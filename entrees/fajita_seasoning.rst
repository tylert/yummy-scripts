Fajita Seasoning
----------------

* 1 tablespoon cornstarch
* 2 teaspoons chili powder
* 1 teaspoon cumin
* 1 teaspoon ground paprika
* 1 teaspoon granulated sugar
* 1 teaspoon salt
* 1/2 teaspoon garlic powder
* 1/2 teaspoon onion powder
* 1/4 teaspoon cayenne pepper


- Add all the ingredients to a bowl and stir well to combine.
- Store it in an air-tight container for up to 6 months.
- Replace a packet of store-bought seasoning with 3 tablespoons of this homemade mix and 1 cup of water

The cornstarch serves dual purposes:
It acts as a moisture absorber, preventing the seasoning mix from clumping.
When used in cooking, it thickens sauces and ensures ingredients are evenly coated with the seasoning.


