Lentil Patties - Vegan
----------------------

* 1 1/2 cups (285g) dried red lentils
* 1 large shallot, finely chopped
* 2 garlic cloves, minced
* 1 small carrot, diced
* 1/4 cup chopped fresh parsley
* 1 tablespoon tomato puree
* 1 teaspoon salt
* 1/2 teaspoon black pepper
* Olive oil

- Rinse the dried red lentils under cold water and drain.
- In a medium-sized pot, bring 3 cups of water to a boil.
- Add the lentils to the boiling water and cook for about 10-15 minutes or until tender. Drain any excess water and set aside.
- In a skillet, heat a drizzle of olive oil over medium heat.
- Add the chopped shallot and minced garlic, sautéing until softened.
- Add the diced carrot to the skillet and cook for another 5 minutes until the carrot is tender. Remove from heat and let cool slightly.
- In a large mixing bowl, combine the cooked lentils, sautéed vegetable mixture, chopped fresh parsley, tomato puree, salt, and black pepper.
- Mix well to combine all the ingredients evenly.
- Divide the mixture into equal portions and shape each portion into a patty, about 1/2 inch thick.
- In a non-stick skillet, heat olive oil over medium heat.
- Add the lentil patties to the skillet and cook for about 4-5 minutes on each side, or until golden brown and crispy.
- Remove the patties from the skillet and place them on a serving plate lined with paper towels to absorb any excess oil.
