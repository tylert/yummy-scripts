Coconut Chutney
---------------

* 1 cup grated coconut
* 4 green chilies
* 2 big pinches curry leaves
* 1/4 tsp salt
* 2 tbsp onion chopped
* 2 tbsp tamarind
* 1-2 tbsp water

- Put all items in a food processor.
- Blend until smooth.
